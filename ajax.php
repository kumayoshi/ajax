<?php
include_once('db_access.php');
// http_response_code(404);


$id = isset($_POST['id']) ? $_POST['id'] : "";
$date = isset($_POST['date'])? $_POST['date'] : "";

$rows = execute_sql(
  'SELECT count(*) AS `cnt` FROM `aj_tb` WHERE `id` = ?;',
  array($id)
);

if($rows[0]['cnt'] == 0){
  $ret = execute_sql(
    'INSERT INTO `aj_tb` (`id`,`date`,`deleted`) VALUES (?,?,?);',
    array($id,$date,false)
  );
} else {
  $ret = execute_sql(
    'UPDATE `aj_tb` SET `date` = ? WHERE id = ?;',
    array($date,$id)
  );
}

$result = array(
  'status' => (int)true,
  // 'id' => $id,
  // 'date' => $date
);

// var_dump($result);

echo json_encode($result);
